<?php

namespace App\Entity;

use App\Repository\CategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CategoryRepository::class)]
class Category
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $name;

    #[ORM\OneToMany(mappedBy: 'category', targetEntity: Product::class, orphanRemoval: true)]
    private $product;

    #[ORM\OneToMany(mappedBy: 'category', targetEntity: UnderCategory::class, orphanRemoval: true)]
    private $underCategories;

    public function __construct()
    {
        $this->product = new ArrayCollection();
        $this->underCategories = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Product>
     */
    public function getProduct(): Collection
    {
        return $this->product;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->product->contains($product)) {
            $this->product[] = $product;
            $product->setCategory($this);
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->product->removeElement($product)) {
            // set the owning side to null (unless already changed)
            if ($product->getCategory() === $this) {
                $product->setCategory(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, UnderCategory>
     */
    public function getUnderCategories(): Collection
    {
        return $this->underCategories;
    }

    public function addUnderCategory(UnderCategory $underCategory): self
    {
        if (!$this->underCategories->contains($underCategory)) {
            $this->underCategories[] = $underCategory;
            $underCategory->setCategory($this);
        }

        return $this;
    }

    public function removeUnderCategory(UnderCategory $underCategory): self
    {
        if ($this->underCategories->removeElement($underCategory)) {
            // set the owning side to null (unless already changed)
            if ($underCategory->getCategory() === $this) {
                $underCategory->setCategory(null);
            }
        }

        return $this;
    }
}
