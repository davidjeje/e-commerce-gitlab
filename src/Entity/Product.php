<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ProductRepository::class)]
class Product
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $band;

    #[ORM\Column(type: 'string', length: 255)]
    private $fullName;

    #[ORM\Column(type: 'date')]
    private $releaseDate;

    #[ORM\Column(type: 'integer')]
    private $stock;

    #[ORM\Column(type: 'string', length: 255)]
    private $image;

    #[ORM\Column(type: 'integer')]
    private $price;

    #[ORM\Column(type: 'text')]
    private $description;

    #[ORM\Column(type: 'boolean')]
    private $isNewProduct;

    #[ORM\Column(type: 'string', length: 255)]
    private $pictureCart;

    #[ORM\ManyToOne(targetEntity: Category::class, inversedBy: 'product')]
    #[ORM\JoinColumn(nullable: false)]
    private $category;

    #[ORM\ManyToOne(targetEntity: UnderCategory::class, inversedBy: 'product')]
    #[ORM\JoinColumn(nullable: false)]
    private $underCategory;

    #[ORM\OneToMany(mappedBy: 'product', targetEntity: Details::class, orphanRemoval: true)]
    private $details;

    #[ORM\OneToMany(mappedBy: 'product', targetEntity: Screen::class, orphanRemoval: true)]
    private $screens;

    #[ORM\OneToMany(mappedBy: 'product', targetEntity: Storage::class, orphanRemoval: true)]
    private $storages;

    #[ORM\OneToMany(mappedBy: 'product', targetEntity: Battery::class, orphanRemoval: true)]
    private $batteries;

    #[ORM\OneToMany(mappedBy: 'product', targetEntity: SizeAndWeight::class, orphanRemoval: true)]
    private $sizeAndWeights;

    #[ORM\OneToMany(mappedBy: 'product', targetEntity: DisplayHead::class, orphanRemoval: true)]
    private $displayHeads;

    #[ORM\OneToMany(mappedBy: 'product', targetEntity: PictureRight::class, orphanRemoval: true)]
    private $pictureRights;

    #[ORM\OneToOne(mappedBy: 'product', targetEntity: Heart::class, cascade: ['persist', 'remove'])]
    private $heart;

    #[ORM\OneToMany(mappedBy: 'product', targetEntity: Software::class, orphanRemoval: true)]
    private $software;

    #[ORM\OneToMany(mappedBy: 'product', targetEntity: Processor::class, orphanRemoval: true)]
    private $processors;

    #[ORM\OneToMany(mappedBy: 'product', targetEntity: Specification::class, orphanRemoval: true)]
    private $specifications;

    #[ORM\OneToMany(mappedBy: 'product', targetEntity: Functionnality::class, orphanRemoval: true)]
    private $functionnalities;

    #[ORM\OneToMany(mappedBy: 'product', targetEntity: OrderDetails::class)]
    private $orderDetails;

    public function __construct()
    {
        $this->details = new ArrayCollection();
        $this->screens = new ArrayCollection();
        $this->storages = new ArrayCollection();
        $this->batteries = new ArrayCollection();
        $this->sizeAndWeights = new ArrayCollection();
        $this->displayHeads = new ArrayCollection();
        $this->pictureRights = new ArrayCollection();
        $this->software = new ArrayCollection();
        $this->processors = new ArrayCollection();
        $this->specifications = new ArrayCollection();
        $this->functionnalities = new ArrayCollection();
        $this->orderDetails = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBand(): ?string
    {
        return $this->band;
    }

    public function setBand(string $band): self
    {
        $this->band = $band;

        return $this;
    }

    public function getFullName(): ?string
    {
        return $this->fullName;
    }

    public function setFullName(string $fullName): self
    {
        $this->fullName = $fullName;

        return $this;
    }

    public function getReleaseDate(): ?\DateTimeInterface
    {
        return $this->releaseDate;
    }

    public function setReleaseDate(\DateTimeInterface $releaseDate): self
    {
        $this->releaseDate = $releaseDate;

        return $this;
    }

    public function getStock(): ?int
    {
        return $this->stock;
    }

    public function setStock(int $stock): self
    {
        $this->stock = $stock;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function isIsNewProduct(): ?bool
    {
        return $this->isNewProduct;
    }

    public function setIsNewProduct(bool $isNewProduct): self
    {
        $this->isNewProduct = $isNewProduct;

        return $this;
    }

    public function getPictureCart(): ?string
    {
        return $this->pictureCart;
    }

    public function setPictureCart(string $pictureCart): self
    {
        $this->pictureCart = $pictureCart;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getUnderCategory(): ?UnderCategory
    {
        return $this->underCategory;
    }

    public function setUnderCategory(?UnderCategory $underCategory): self
    {
        $this->underCategory = $underCategory;

        return $this;
    }

    /**
     * @return Collection<int, Details>
     */
    public function getDetails(): Collection
    {
        return $this->details;
    }

    public function addDetail(Details $detail): self
    {
        if (!$this->details->contains($detail)) {
            $this->details[] = $detail;
            $detail->setProduct($this);
        }

        return $this;
    }

    public function removeDetail(Details $detail): self
    {
        if ($this->details->removeElement($detail)) {
            // set the owning side to null (unless already changed)
            if ($detail->getProduct() === $this) {
                $detail->setProduct(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Screen>
     */
    public function getScreens(): Collection
    {
        return $this->screens;
    }

    public function addScreen(Screen $screen): self
    {
        if (!$this->screens->contains($screen)) {
            $this->screens[] = $screen;
            $screen->setProduct($this);
        }

        return $this;
    }

    public function removeScreen(Screen $screen): self
    {
        if ($this->screens->removeElement($screen)) {
            // set the owning side to null (unless already changed)
            if ($screen->getProduct() === $this) {
                $screen->setProduct(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Storage>
     */
    public function getStorages(): Collection
    {
        return $this->storages;
    }

    public function addStorage(Storage $storage): self
    {
        if (!$this->storages->contains($storage)) {
            $this->storages[] = $storage;
            $storage->setProduct($this);
        }

        return $this;
    }

    public function removeStorage(Storage $storage): self
    {
        if ($this->storages->removeElement($storage)) {
            // set the owning side to null (unless already changed)
            if ($storage->getProduct() === $this) {
                $storage->setProduct(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Battery>
     */
    public function getBatteries(): Collection
    {
        return $this->batteries;
    }

    public function addBattery(Battery $battery): self
    {
        if (!$this->batteries->contains($battery)) {
            $this->batteries[] = $battery;
            $battery->setProduct($this);
        }

        return $this;
    }

    public function removeBattery(Battery $battery): self
    {
        if ($this->batteries->removeElement($battery)) {
            // set the owning side to null (unless already changed)
            if ($battery->getProduct() === $this) {
                $battery->setProduct(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, SizeAndWeight>
     */
    public function getSizeAndWeights(): Collection
    {
        return $this->sizeAndWeights;
    }

    public function addSizeAndWeight(SizeAndWeight $sizeAndWeight): self
    {
        if (!$this->sizeAndWeights->contains($sizeAndWeight)) {
            $this->sizeAndWeights[] = $sizeAndWeight;
            $sizeAndWeight->setProduct($this);
        }

        return $this;
    }

    public function removeSizeAndWeight(SizeAndWeight $sizeAndWeight): self
    {
        if ($this->sizeAndWeights->removeElement($sizeAndWeight)) {
            // set the owning side to null (unless already changed)
            if ($sizeAndWeight->getProduct() === $this) {
                $sizeAndWeight->setProduct(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, DisplayHead>
     */
    public function getDisplayHeads(): Collection
    {
        return $this->displayHeads;
    }

    public function addDisplayHead(DisplayHead $displayHead): self
    {
        if (!$this->displayHeads->contains($displayHead)) {
            $this->displayHeads[] = $displayHead;
            $displayHead->setProduct($this);
        }

        return $this;
    }

    public function removeDisplayHead(DisplayHead $displayHead): self
    {
        if ($this->displayHeads->removeElement($displayHead)) {
            // set the owning side to null (unless already changed)
            if ($displayHead->getProduct() === $this) {
                $displayHead->setProduct(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, PictureRight>
     */
    public function getPictureRights(): Collection
    {
        return $this->pictureRights;
    }

    public function addPictureRight(PictureRight $pictureRight): self
    {
        if (!$this->pictureRights->contains($pictureRight)) {
            $this->pictureRights[] = $pictureRight;
            $pictureRight->setProduct($this);
        }

        return $this;
    }

    public function removePictureRight(PictureRight $pictureRight): self
    {
        if ($this->pictureRights->removeElement($pictureRight)) {
            // set the owning side to null (unless already changed)
            if ($pictureRight->getProduct() === $this) {
                $pictureRight->setProduct(null);
            }
        }

        return $this;
    }

    public function getHeart(): ?Heart
    {
        return $this->heart;
    }

    public function setHeart(Heart $heart): self
    {
        // set the owning side of the relation if necessary
        if ($heart->getProduct() !== $this) {
            $heart->setProduct($this);
        }

        $this->heart = $heart;

        return $this;
    }

    /**
     * @return Collection<int, Software>
     */
    public function getSoftware(): Collection
    {
        return $this->software;
    }

    public function addSoftware(Software $software): self
    {
        if (!$this->software->contains($software)) {
            $this->software[] = $software;
            $software->setProduct($this);
        }

        return $this;
    }

    public function removeSoftware(Software $software): self
    {
        if ($this->software->removeElement($software)) {
            // set the owning side to null (unless already changed)
            if ($software->getProduct() === $this) {
                $software->setProduct(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Processor>
     */
    public function getProcessors(): Collection
    {
        return $this->processors;
    }

    public function addProcessor(Processor $processor): self
    {
        if (!$this->processors->contains($processor)) {
            $this->processors[] = $processor;
            $processor->setProduct($this);
        }

        return $this;
    }

    public function removeProcessor(Processor $processor): self
    {
        if ($this->processors->removeElement($processor)) {
            // set the owning side to null (unless already changed)
            if ($processor->getProduct() === $this) {
                $processor->setProduct(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Specification>
     */
    public function getSpecifications(): Collection
    {
        return $this->specifications;
    }

    public function addSpecification(Specification $specification): self
    {
        if (!$this->specifications->contains($specification)) {
            $this->specifications[] = $specification;
            $specification->setProduct($this);
        }

        return $this;
    }

    public function removeSpecification(Specification $specification): self
    {
        if ($this->specifications->removeElement($specification)) {
            // set the owning side to null (unless already changed)
            if ($specification->getProduct() === $this) {
                $specification->setProduct(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Functionnality>
     */
    public function getFunctionnalities(): Collection
    {
        return $this->functionnalities;
    }

    public function addFunctionnality(Functionnality $functionnality): self
    {
        if (!$this->functionnalities->contains($functionnality)) {
            $this->functionnalities[] = $functionnality;
            $functionnality->setProduct($this);
        }

        return $this;
    }

    public function removeFunctionnality(Functionnality $functionnality): self
    {
        if ($this->functionnalities->removeElement($functionnality)) {
            // set the owning side to null (unless already changed)
            if ($functionnality->getProduct() === $this) {
                $functionnality->setProduct(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, OrderDetails>
     */
    public function getOrderDetails(): Collection
    {
        return $this->orderDetails;
    }

    public function addOrderDetail(OrderDetails $orderDetail): self
    {
        if (!$this->orderDetails->contains($orderDetail)) {
            $this->orderDetails[] = $orderDetail;
            $orderDetail->setProduct($this);
        }

        return $this;
    }

    public function removeOrderDetail(OrderDetails $orderDetail): self
    {
        if ($this->orderDetails->removeElement($orderDetail)) {
            // set the owning side to null (unless already changed)
            if ($orderDetail->getProduct() === $this) {
                $orderDetail->setProduct(null);
            }
        }

        return $this;
    }
}
