<?php

namespace App\Repository;

use App\Entity\SizeAndWeight;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<SizeAndWeight>
 *
 * @method SizeAndWeight|null find($id, $lockMode = null, $lockVersion = null)
 * @method SizeAndWeight|null findOneBy(array $criteria, array $orderBy = null)
 * @method SizeAndWeight[]    findAll()
 * @method SizeAndWeight[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SizeAndWeightRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SizeAndWeight::class);
    }

    public function add(SizeAndWeight $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(SizeAndWeight $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return SizeAndWeight[] Returns an array of SizeAndWeight objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('s.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?SizeAndWeight
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
